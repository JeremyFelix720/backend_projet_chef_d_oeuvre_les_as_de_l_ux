import { Router, Response, Request } from "express";
import { Project } from "../index";
import "dotenv/config";
import { checkToken } from "../middlewares/checkToken";

export const projectRouter = Router();

// Ajout d'un projet
projectRouter.post("/add", async (req, res) => {
  //console.log(req);

  //const projectId = req.body.id;
  const projectTitle = req.body.title;
  const projectBannerUrl = req.body.bannerUrl;
  const projectWebsiteUrl = req.body.websiteUrl;
  const projectDescription = req.body.description;
  const projectBudget = req.body.budget;
  // const projectComments = req.body.comments;
  // const projectMaximumComments = req.body.maximumComments;

  const newProject = await Project.create({
    // id: projectId,
    title: projectTitle,
    bannerUrl: projectBannerUrl,
    websiteUrl: projectWebsiteUrl,
    description: projectDescription,
    budget: projectBudget,
    comments: 0,
    maximumComments: projectBudget,
  });

  res.status(200).json(
    {
      message: "Le projet a bien été rajouté.",
      ...newProject // destructuration de l'objet newProject qui correspond à aux lignes suivantes :
      // id: projectId,
      /*
      title: projectTitle,
      bannerUrl: projectBannerUrl,
      websiteUrl: projectWebsiteUrl,
      description: projectDescription,
      budget: projectBudget,
      comments: projectComments,
      maximumComments: projectMaximumComments,
      */
    }
  )
})

// Suppression d'un projet (pour un utilisateur connecté)
projectRouter.delete("/:id", checkToken,async (req, res) => {
  const savedProject = await Project.findOne({ where: { id: req.params.id } });
  if (savedProject) {
      await savedProject.destroy();
      res.end("Project deleted");
  }
  else {
      res.end("Project not found");
  }
});

// Lecture des projets
projectRouter.get("/", async (req, res) => {
  const savedProjects = await Project.findAll();
  res.json(savedProjects);
});

// Lecture d'un projet
projectRouter.get("/:id", async (req, res) => {
  const projectId = req.params.id;
  const savedProject = await Project.findByPk(projectId);
  res.status(200).json(savedProject);
})

// Modification d'un projet
projectRouter.put("/:id", async (req, res) => {
  const projectId = req.params.id;
  const projectTitle = req.body.title;
  const projectBannerUrl = req.body.bannerUrl;
  const projectWebsiteUrl = req.body.websiteUrl;
  const projectDescription = req.body.description;
  const projectBudget = req.body.budget;

  const projectModified = {
    title: projectTitle,
    bannerUrl: projectBannerUrl,
    websiteUrl: projectWebsiteUrl,
    description: projectDescription,
    budget: projectBudget,
    maximumComments: projectBudget
  };

  await Project.update(projectModified, {where:
      {id: projectId}
    });
  res.status(200).json(projectModified);
})
